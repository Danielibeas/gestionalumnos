/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionalumnos;

import java.util.Objects;

/**
 *
 * @author alumno
 */
public class Persona implements Cloneable, Comparable<Persona> {
    //Atributos
    protected Nif nif;
    protected String nombre;
    protected int edad;
    
    //Constructor
    public Persona(Nif nif, String nombre, int edad) {
        //System.out.println("Empezando a construir Persona");
        this.nif=nif;
        this.nombre = nombre;
        this.edad = edad;
        //System.out.println("Fin del constructor de Persona");      
    }

    //Métodos
    public Nif getNif() {
        return nif;
    }
    
    public String getNombre() {
        return nombre;
    }

    public int getEdad() {
        return edad;
    }
    
    public void setEdad(int e){
        edad=e;
    }

    /*
    @Override
    public boolean equals(Object o) {
        Persona p=(Persona)o;
        return this.nombre.equals(p.nombre);
        //To change body of generated methods, choose Tools | Templates.
    }
    */

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + Objects.hashCode(this.nif);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Persona other = (Persona) obj;
        if (!Objects.equals(this.nif, other.nif)) {
            return false;
        }
        return true;
    }

    

    @Override
    public String toString() {
        return "Persona{" + "nombre=" + nombre + ", edad=" + edad + '}';
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize(); //To change body of generated methods, choose Tools | Templates.
    }

    /*
    @Override
    protected Persona clone(){
        return new Persona(nombre, edad); //To change body of generated methods, choose Tools | Templates.
    }
    */
    
    @Override
    public Persona clone() throws CloneNotSupportedException{
        return (Persona) super.clone();
    }

    @Override
    public int compareTo(Persona p) {
        //return this.nombre.compareTo(t.nombre); //To change body of generated methods, choose Tools | Templates.
        return this.nif.compareTo(p.nif);
    }
    
    
}
