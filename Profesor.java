/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionalumnos;

/**
 *
 * @author alumno
 */
public class Profesor extends Persona{
    //Atributos
    private String especialidad;
    private double sueldo;
    //Constructor

    public Profesor(Nif nif, String nombre, int edad, String especialidad, double sueldo) {
        super(nif, nombre, edad);
        this.especialidad = especialidad;
        this.sueldo = sueldo;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public double getSueldo() {
        return sueldo;
    }
    
    @Override
    public String toString() {
        //return "Alumno{" +"Nombre="+nombre+", Edad="+edad +", curso=" + curso + ", nivelAcademico=" + nivelAcademico + '}';
        return super.toString()+ " Alumno{"+"nif="+nif+"especialidad=" + especialidad + ", sueldo=" + sueldo + '}';
    }
}
