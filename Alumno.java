/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionalumnos;

/**
 *
 * @author alumno
 */
public class Alumno extends Persona implements Cloneable{
    //Atributos
    private int curso;
    private String nivelAcademico;

    //Constructor
    public Alumno(Nif nif, String nombre, int edad, int curso, String nivelAcademico) {
        super(nif, nombre, edad);
        //System.out.println("Inicializando los atributos del alumno");      
        this.curso = curso;
        this.nivelAcademico = nivelAcademico;
        //System.out.println("Fin del constructor de alumno");
    }
    
    //Métodos
    public int getCurso() {
        return curso;
    }

    public void setCurso(int curso) {
        this.curso = curso;
    }

    public void setNivelAcademico(String nivelAcademico) {
        this.nivelAcademico = nivelAcademico;
    }

    
    
    public String getNivelAcademico() {
        return nivelAcademico;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize(); //To change body of generated methods, choose Tools | Templates.
    }

    /*
    @Override
    protected Alumno clone(){
        Alumno a; //To change body of generated methods, choose Tools | Templates.
        a=new Alumno(nombre,edad,curso,nivelAcademico);
        return a;
    }*/
    
    @Override
    public Alumno clone() throws CloneNotSupportedException{
        return (Alumno) super.clone();
    }
    
    @Override
    public String toString() {
        //return "Alumno{" +"Nombre="+nombre+", Edad="+edad +", curso=" + curso + ", nivelAcademico=" + nivelAcademico + '}';
        return super.toString()+ " Alumno{"+"nif="+nif+"curso=" + curso + ", nivelAcademico=" + nivelAcademico + '}';
    }
    
    
}
