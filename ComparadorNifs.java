/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionalumnos;

import java.util.Comparator;

/**
 *
 * @author alumno
 */
public class ComparadorNifs implements Comparator<Persona>{

    @Override
    public int compare(Persona pp1, Persona pp2) {
        return pp1.nif.compareTo(pp2.nif);
    }
    
}
