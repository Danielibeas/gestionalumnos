package gestionalumnos;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Nif implements Comparable<Nif> {
		//Atributos
		private String numero;
		private char letra;
		private GregorianCalendar fecha_expedicion;
		
		private GregorianCalendar fecha_caducidad;
		//Contructores
		public Nif(String nif, GregorianCalendar fecha_expedicion){
			if(isNif(nif)==false){
				this.numero=null;
				this.fecha_expedicion=null;
				this.fecha_caducidad=null;
			}else{
				this.numero=nif.substring(0, nif.length()-1);
				this.letra=Character.toUpperCase(nif.substring(nif.length()-1, nif.length()).charAt(0));
				this.fecha_expedicion=fecha_expedicion;
				this.fecha_caducidad=(GregorianCalendar)fecha_expedicion.clone();
					
				this.fecha_caducidad.add(Calendar.YEAR, 5);
			}
		}
		//Metodos
		public static char calcularLetra(int numero){
			final char [] LETRAS= {'T','R','W','A','G','M','Y','F','P','D','X', 'B','N','J','Z','S','Q','V','H','L', 'C','K','E'};
			return LETRAS[numero%23];
		}
		public boolean isCaducado(){
			GregorianCalendar hoy=new GregorianCalendar();
			if(fecha_caducidad.after(hoy)){
				return false;
			}
			return true;
		}
		
		public static boolean isNif(String nif){
			String numero=nif.substring(0, nif.length()-1);
			if(numero.length()!=7 && numero.length()!=8){
				return false;
			}else if(numero.length()==7 && numero.startsWith("0")==false){
				return false;
			}
			int num;
			try{
				num=Integer.parseInt(numero);
				
			}catch(Exception e){
				return false;
			}
			System.out.println();
			char letra=Character.toUpperCase(nif.substring(nif.length()-1,nif.length()).charAt(0));
			if(letra!=calcularLetra(num)){
				return false;
			}
			return true;
		}
		
		public String getNumero() {
			return numero;
		}
		public char getLetra() {
			return letra;
		}
		public GregorianCalendar getFecha_expedicion() {
			return fecha_expedicion;
		}
		public GregorianCalendar getFecha_caducidad() {
			return fecha_caducidad;
		}
		
		public void setNumero(String numero) {
			if(numero.length()!=7 && numero.length()!=8){
				this.numero=null;
			}else if(numero.length()==7 && numero.startsWith("0")==false){
				this.numero= null;
			}
			int num;
			try{
				num=Integer.parseInt(numero);
				this.letra= this.calcularLetra(num);
				
			}catch(Exception e){
				this.numero=null;
			}
			
		}
		public void setFecha_expedicion(GregorianCalendar fecha_expedicion) {
			this.fecha_expedicion = fecha_expedicion;
		}
		
		@Override
		public boolean equals(Object n){
			Nif ni=(Nif)n;
			return this.numero.equals(ni.getNumero()) && letra==ni.getLetra();
		}
		
		@Override
		public String toString(){
			return "Nif["+numero+", "+letra+"]";
		}

    @Override
    public int compareTo(Nif n) {
        return (numero+letra).compareTo(n.numero+n.letra);
    }
}
