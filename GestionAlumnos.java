/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionalumnos;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Scanner;

/**
 *
 * @author alumno
 */
public class GestionAlumnos {

    public final static int NUEVO = 1;
    public final static int BORRAR = 2;
    public final static int MODIFICAR = 3;
    public final static int LISTAR = 4;
    public final static int CONSULTAR = 5;
    public final static int ORDENAR = 6;
    public final static int FIN = 7;
    public static Scanner teclado = new Scanner(System.in);

    public static int menu() {      
        System.out.println("GESTION DE ALUMNOS");
        System.out.println("------------------");
        System.out.println("1. Nuevo");
        System.out.println("2. Borrar");
        System.out.println("3. Modificar");
        System.out.println("4. Listar");
        System.out.println("5. Consultar");
        System.out.println("6. Ordenar");
        System.out.println("7. Fin");
        System.out.println("Selecciona una opción");
        int opcion = Integer.parseInt(teclado.nextLine());
        while (opcion != NUEVO && opcion != BORRAR && opcion != MODIFICAR
                && opcion != LISTAR && opcion != CONSULTAR && 
                opcion != ORDENAR && opcion != FIN) {
            System.out.println("Error, opción no válida");
            opcion = Integer.parseInt(teclado.nextLine());
        }
        return opcion;
    }

    public static String teclearNif(String mensaje) {
        System.out.println(mensaje);
        String sNif = teclado.nextLine();
        while (Nif.isNif(sNif) == false) {
            System.out.println("Error, NIF no válido");
            System.out.println(mensaje);
            sNif = teclado.nextLine();
        }
        return sNif;
    }

    public static GregorianCalendar getGregorianCalendar(String fecha) {
        String[] datos;
        if (!fecha.contains("/")) {
            datos = fecha.split("-");
        } else {
            datos = fecha.split("/");
        }
        int d = Integer.parseInt(datos[0]);
        int m = Integer.parseInt(datos[1]);
        int a = Integer.parseInt(datos[2]);
        return new GregorianCalendar(a, m - 1, d);
    }

    public static void main(String[] args) {
        ArrayList<Alumno> alumnos = new ArrayList();
        alumnos.add(new Alumno(
                new Nif("12345678Z", getGregorianCalendar("21/12/2012")),
                "Pepe García", 25, 1, "DAW"));
        alumnos.add(new Alumno(
                new Nif("12333444Q", getGregorianCalendar("22/2/2014")),
                "Ana Martínez", 22, 2, "DAW"));
        int opcion = menu();
        System.out.println("Opcion seleccionada " + opcion);
        Alumno a;
        while (opcion != FIN) {
            switch (opcion) {
                case NUEVO:
                    //INTRODUCIR POR TECLADO LOS DATOS DEL ALUMNO
                    String mensaje="Teclea el NIF nuevo:";
                    String sNif = teclearNif(mensaje);
                    System.out.println("Fecha expedicion NIF:");
                    GregorianCalendar fExpedicion = getGregorianCalendar(teclado.nextLine());
                    Nif nif = new Nif(sNif, fExpedicion);
                    System.out.println("Nombre:");
                    String nombre = teclado.nextLine();
                    System.out.println("Edad:");
                    int edad = Integer.parseInt(teclado.nextLine());
                    System.out.println("Curso:");
                    int curso = Integer.parseInt(teclado.nextLine());
                    System.out.println("Nivel académico:");
                    String nivel = teclado.next();
                    //CREAR EL OBJETO ALUMNO
                    a = new Alumno(nif, nombre, edad, curso, nivel);
                    //AÑADIR A LA LISTA EL OBJETO ALUMNO
                    alumnos.add(a);
                    break;
                case BORRAR:
                    mensaje="Teclea el NIF que deseas borrar:";
                    sNif = teclearNif(mensaje);
                    boolean existe = false;
                    for (Alumno al : alumnos) {
                        if (sNif.equals(al.getNif().getNumero() + al.getNif().getLetra())) {
                            alumnos.remove(al);
                            existe = true;
                            break;
                        }
                    }
                    if (existe == true) {
                        System.out.println("El alumno de NIF= " + sNif + "ha sido borrado");
                    } else {
                        System.out.println("No hay ningún alumno en la lista con el NIF=" + sNif);
                    }

                    break;
                case MODIFICAR:
                    mensaje="Teclea el NIF que deseas modificar:";
                    sNif = teclearNif(mensaje);
                    existe = false;
                    for (Alumno al : alumnos) {
                        if (sNif.equals(al.getNif().getNumero() + al.getNif().getLetra())) {
                            System.out.println("¿Que dato quieres modificar (1=Edad, 2=curso ó 3=Nivel educativo )? ");
                            int campo=Integer.parseInt(teclado.nextLine());
                            switch(campo){
                                case 1://edad
                                    System.out.println("Su edad ahora es "+al.getEdad());
                                    System.out.println("Teclea su nueva edad:");
                                    al.setEdad(Integer.parseInt(teclado.nextLine()));
                                    break;
                                case 2://curso
                                    break;
                                case 3://nivel educativo
                                    break;
                                default:
                                    System.out.println("Ese campo no existe o no se puede modificar");
                            }
                            existe = true;
                            break;
                        }
                    }
                    if (existe == true) {
                        System.out.println("El alumno de NIF= " + sNif + "ha sido borrado");
                    } else {
                        System.out.println("No hay ningún alumno en la lista con el NIF=" + sNif);
                    }

                    
                    break;
                case LISTAR:
                    System.out.println("Listado de alumnos");
                    System.out.println("------------------");
                    alumnos.stream().forEach((al) -> {
                        System.out.println(al);
            });
                    break;
                case CONSULTAR:
                    mensaje="Teclea el NIF que deseas consultar:";
                    sNif = teclearNif(mensaje);
                    existe = false;
                    for (Alumno al : alumnos) {
                        if (sNif.equals(al.getNif().getNumero() + al.getNif().getLetra())) {
                            System.out.println(al);
                            existe = true;
                            break;
                        }
                    }
                    if (existe == true) {

                    } else {
                        System.out.println("No hay ningún alumno en la lista con el NIF= " + sNif);
                    }
                    break;
                case ORDENAR:
                    System.out.println("¿Por que campo quieres ordenar 1=NIF, 2=Nombre ó 3=Edad ?");
                    int campo=Integer.parseInt(teclado.nextLine());
                            switch(campo){
                                case 1://ordenar por NIF
                                    alumnos.sort(new ComparadorNifs());
                                    break;
                                case 2://
                                    break;
                                case 3:
                                    break;
                            }
                    break;
            }
            opcion = menu();
        }
    }
}
